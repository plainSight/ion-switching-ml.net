using Microsoft.ML.Data;

namespace IonPrediction
{
    public class Ion
    {
        /// <summary>
        /// Значение времени в конкретный момент.
        /// </summary>
        [LoadColumn(0)]
        public float Time;

        /// <summary>
        /// Значение силы пропускаемого тока в конкретный момент.
        /// </summary>
        [LoadColumn(1)]
        public float Signal;

        /// <summary>
        /// Количество открытых каналов в конкретный момент.
        /// </summary>
        [LoadColumn(2)]
        public int Channels;
    }
}
