using Microsoft.ML.Data;

namespace IonPrediction
{
    public class Prediction
    {
        /// <summary>
        /// Спрогнозированное количество открытых каналов в конкретный момент.
        /// </summary>
        [ColumnName("PredictedLabel")]
        public int Channels;
    }
}
