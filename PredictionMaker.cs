using System;
using Microsoft.ML;
using Microsoft.ML.Data;
using System.IO;

namespace IonPrediction
{
    class PredictionMaker
    {
        /// <summary>
        /// Адрес файла с данными для прогноза.
        /// </summary>
        const string INPUT_ADDRESS = @"../../../../Data/PreparedData/test.csv";

        /// <summary>
        /// Адрес для результата выполнения программы.
        /// </summary>
        const string OUTPUT_ADDRESS = @"../../../../Data/ResultData/result.csv";

        /// <summary>
        /// Общий контекст для операций в ML.
        /// </summary>
        static MLContext mlContext = new MLContext(seed: 0);

        /// <summary>
        /// Объект для загрузки данных из файлов в IDataView.
        /// </summary>
        static TextLoader textLoader = mlContext.Data.CreateTextLoader<Ion>(separatorChar: ',', hasHeader: true);

        /// <summary>
        /// Основной метод программы.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            // Сгенерируем массив с адресами файлов, содержащих входные данные.
            string[] addresses = new string[10];
            for (int i = 0; i < 10; i++)
                addresses[i] = @$"../../../../Data/PreparedData/train{i + 1}.csv";

            PredictionEngine<Ion, Prediction> predictionEngine1LP = null,
                                              predictionEngine1HP = null,
                                              predictionEngine3 = null,
                                              predictionEngine5 = null,
                                              predictionEngine10 = null;

            try
            {
                // Данные с 1 открывающимся каналом и низкой интенсивностью открытия (LP - low probability).
                predictionEngine1LP = MakePredictionEngineFromData(addresses[0], addresses[1], 1);
                Console.WriteLine("Модель для 1 канала (низкая интенсивность) создана.");
                // Данные с 1 открывающимся каналом и высокой интенсивностью открытия (HP - high probability).
                predictionEngine1HP = MakePredictionEngineFromData(addresses[2], addresses[6], 1);
                Console.WriteLine("Модель для 1 канала (высокая интенсивность) создана.");
                // Данные с 3 открывающимися каналами.
                predictionEngine3 = MakePredictionEngineFromData(addresses[3], addresses[7], 3);
                Console.WriteLine("Модель для 3 каналов создана.");
                // Данные с 5 открывающимися каналами.
                predictionEngine5 = MakePredictionEngineFromData(addresses[5], addresses[8], 5);
                Console.WriteLine("Модель для 5 каналов создана.");
                // Данные с 10 открывающимися каналами.
                predictionEngine10 = MakePredictionEngineFromData(addresses[4], addresses[9], 10);
                Console.WriteLine("Модель для 10 каналов создана.");
            }
            catch (IOException)
            {
                Console.WriteLine("Ошибка чтения файла. Проверьте наличие и настройки доступа входных файлов.");
                Console.WriteLine("Нажмите любую клавишу...");
                Console.ReadKey(true);
                return;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Во время создания объекта PredictionEngine произошла непредвиденная ошибка.");
                Console.WriteLine("Информация об ошибке:");
                Console.WriteLine(ex.Message);
                Console.WriteLine("Нажмите любую клавишу...");
                Console.ReadKey(true);
                return;
            }

            // Будем каждую строку файла test.csv превращать в объект класса Ion, совершать прогноз
            // и загружать результат в массив строк, который будет выгружен в формате, соответствующем требованию задачи.
            string[] lines = null;
            try
            {
                lines = File.ReadAllLines(INPUT_ADDRESS);
            }
            catch (Exception)
            {
                Console.WriteLine($"Ошибка при чтении файла. Проверьте наличие и настройки доступа файла {INPUT_ADDRESS}.");
                Console.WriteLine("Нажмите любую клавишу...");
                Console.ReadKey(true);
                return;
            }
            string[] result = new string[lines.Length];
            result[0] = "time,open_channels";
            Console.WriteLine("Начинаем прогноз:");
            for (int i = 1; i < lines.Length; i++)
            {
                string[] vals = lines[i].Split(',');
                Ion ion = new Ion
                {
                    Time = float.Parse(vals[0].Replace('.', ',')),
                    Signal = float.Parse(vals[1].Replace('.', ','))
                };
                result[i] = vals[0] + ",";
                // Будем для каждых 10 секунд данных (соответственно значению Time) использовать соответствующую виду этих данных модель.
                // Выражение соответствия имеет такой вид потому, что данные в test.csv начинаются со значения Time = 500.0001
                switch ((int)ion.Time / 10 - 50)
                {
                    case 0:
                        result[i] += MakePrediction(predictionEngine1LP, ion);
                        break;
                    case 1:
                        result[i] += MakePrediction(predictionEngine3, ion);
                        break;
                    case 2:
                        result[i] += MakePrediction(predictionEngine5, ion);
                        break;
                    case 3:
                        result[i] += MakePrediction(predictionEngine1LP, ion);
                        break;
                    case 4:
                        result[i] += MakePrediction(predictionEngine1HP, ion);
                        break;
                    case 5:
                        result[i] += MakePrediction(predictionEngine10, ion);
                        break;
                    case 6:
                        result[i] += MakePrediction(predictionEngine5, ion);
                        break;
                    case 7:
                        result[i] += MakePrediction(predictionEngine10, ion);
                        break;
                    case 8:
                        result[i] += MakePrediction(predictionEngine1LP, ion);
                        break;
                    case 9:
                        result[i] += MakePrediction(predictionEngine3, ion);
                        break;
                    default:
                        result[i] += MakePrediction(predictionEngine1LP, ion);
                        break;
                }
                // Уведомление каждую секунду данных (Time), что все данные вплоть до строки с таким значением Time были обработаны.
                if (ion.Time * 10000 % 10000 == 0)
                    Console.WriteLine(ion.Time + " сек ОК");
            }
            // Уведомление, что прогноз данных завершен.
            Console.WriteLine("Прогноз готов.");
            try
            {
                File.WriteAllLines(OUTPUT_ADDRESS, result);
                Console.WriteLine($"Результат записан в файл {OUTPUT_ADDRESS}.");
                Console.WriteLine("Нажмите любую клавишу...");
                Console.ReadKey(true);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Не удалось загрузить результат в файл. Проверьте настройки доступа файла {OUTPUT_ADDRESS}.");
                Console.WriteLine("Информация об ошибке:");
                Console.WriteLine(ex.Message);
                Console.WriteLine("Нажмите любую клавишу...");
                Console.ReadKey(true);
            }
        }

        /// <summary>
        /// Метод для создания прогнозирующей модели.
        /// </summary>
        /// <param name="data">Набор данных для обучения модели.</param>
        /// <param name="channels">Количество открывающихся каналов.</param>
        /// <returns></returns>
        static ITransformer CreateModel(IDataView data, int channels)
        {
            var pipeline = ProcessData(channels);
            //pipeline = BuildAndTrainModel(data, pipeline);
            var model = pipeline.Fit(data);
            return model;
        }

        /// <summary>
        /// Метод для создания объектов класса PredictionEngine (для дальнейших единичных прогнозов).
        /// </summary>
        /// <param name="firstAddress">Адрес файла с первым набором данных.</param>
        /// <param name="secondAddress">Адрес файла со вторым набором данных.</param>
        /// <param name="channels">Количество открывающихся каналов.</param>
        /// <returns></returns>
        static PredictionEngine<Ion, Prediction> MakePredictionEngineFromData(string firstAddress, string secondAddress, int channels)
        {
            IDataView data = textLoader.Load(firstAddress, secondAddress);
            var model = CreateModel(data, channels);
            return mlContext.Model.CreatePredictionEngine<Ion, Prediction>(model);
        }

        /// <summary>
        /// Создание конвейера для преобразования данных и использования алгоритма обучения.
        /// </summary>
        /// <param name="channels">Количество открывающихся каналов.</param>
        /// <returns>Конвейер для преобразования данных.</returns>
        static IEstimator<ITransformer> ProcessData(int channels)
        {
            // Используется алгоритм LGBM для многоклассовой классификации.
            // Ограничим количество классов до количества открывающихся каналов + 1 (когда все каналы закрыты) и будем строить только одно дерево.
            var pipeline = mlContext.Transforms.Concatenate("Features", "Signal")
                .Append(mlContext.Transforms.Conversion.MapValueToKey(inputColumnName: "Channels", outputColumnName: "Label", maximumNumberOfKeys: channels + 1))
                .Append(mlContext.MulticlassClassification.Trainers.LightGbm(labelColumnName: "Label", featureColumnName: "Features", numberOfLeaves: channels + 1 - 3 * (channels / 10), numberOfIterations: 1))
                .Append(mlContext.Transforms.Conversion.MapKeyToValue("PredictedLabel"));
            return pipeline;
        }

        /// <summary>
        /// Метод для совершения одного прогноза.
        /// </summary>
        /// <param name="predictionEngine">Объект PredictionEngine для совершения единичных прогнозов.</param>
        /// <param name="ion">Объект, для которого совершается прогноз.</param>
        /// <returns>Результат прогноза (количество открытых каналов).</returns>
        static int MakePrediction(PredictionEngine<Ion, Prediction> predictionEngine, Ion ion)
        {
            return predictionEngine.Predict(ion).Channels;
        }
    }
}
