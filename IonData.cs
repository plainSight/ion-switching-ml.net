using System.IO;

namespace IonDataTreatment
{
    public class IonData
    {
        /// <summary>
        /// Массив значений времени в каждый i-й момент.
        /// </summary>
        public double[] Time { get; set; }

        /// <summary>
        /// Массив значений силы пропускаемого тока в каждый i-й момент.
        /// </summary>
        public double[] Signal { get; set; }

        /// <summary>
        /// Массив количества открытых каналов в каждый i-й момент.
        /// </summary>
        public uint[] OpenChannels { get; set; }

        /// <summary>
        /// Конструктор без параметров.
        /// </summary>
        public IonData()
        {
        }

        /// <summary>
        /// Метод, осуществляющий чтение данных из файла.
        /// </summary>
        /// <param name="address">Адрес файла для чтения данных.</param>
        public void ReadFromFile(string address)
        {
            // Приведем значения с плавающей запятой к формату с плавающей точкой.
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            string[] data = File.ReadAllLines(address);
            Time = new double[data.Length - 1];
            // Значения силы тока будут обрабатываться только в случае их присутствия в файле.
            Signal = new double[data.Length - 1];
            if (data[0].Split(',').Length == 3)
                OpenChannels = new uint[data.Length - 1];
            for (int i = 1; i < data.Length; i++)
            {
                string[] values = data[i].Split(',');
                Time[i - 1] = double.Parse(values[0]);
                Signal[i - 1] = double.Parse(values[1]);
                if (values.Length == 3)
                    OpenChannels[i - 1] = uint.Parse(values[2]);
            }
        }

        /// <summary>
        /// Метод для устранения линейного сдвига.
        /// </summary>
        /// <param name="leftBound">Индекс первого элемента отрезка со сдвигом.</param>
        /// <param name="rightBound">Индекс последнего элемента отрезка со сдвигом.</param>
        /// <param name="delta">Разница значений на краях отрезка.</param>
        public void RemoveLinearDrift(int leftBound, int rightBound, double delta)
        {
            for (int i = leftBound; i < rightBound; i++)
            {
                Signal[i] -= (i - leftBound) * delta / (rightBound - leftBound);
            }
        }

        /// <summary>
        /// Метод для устранения параболического сдвига.
        /// </summary>
        /// <param name="leftBound">Индекс первого элемента отрезка со сдвигом.</param>
        /// <param name="rightBound">Индекс последнего элемента отрезка со сдвигом.</param>
        /// <param name="deltaSignal">Высота параболы.</param>
        public void RemoveParabolicDrift(int leftBound, int rightBound, double deltaSignal)
        {
            double deltaTime = (Time[rightBound - 1] - Time[leftBound] + 0.0001) / 2;
            double averageTime = Time[leftBound] + deltaTime;
            for (int i = leftBound; i < rightBound; i++)
            {
                Signal[i] += (deltaSignal / (deltaTime * deltaTime)) * (Time[i] - averageTime) * (Time[i] - averageTime) - deltaSignal;
            }
        }

        /// <summary>
        /// Метод, осуществляющий выгрузку данных в файл.
        /// </summary>
        /// <param name="address">Адрес файла для записи данных.</param>
        public void LoadToFile(string address)
        {
            string[] lines = new string[Signal.Length + 1];
            lines[0] = "time,signal";
            // Если есть значения открытых каналов, они тоже должны быть записаны.
            if (OpenChannels != null)
                lines[0] += ",open_channels";
            for (int i = 0; i < Signal.Length; i++)
            {
                lines[i + 1] = $"{Time[i].ToString("f4")},{Signal[i].ToString("f4")}";
                if (OpenChannels != null)
                    lines[i + 1] += $",{OpenChannels[i]}";
            }
            File.WriteAllLines(address, lines);
        }

        /// <summary>
        /// Метод, осуществляющий выгрузку части данных в файл.
        /// </summary>
        /// <param name="address">Адрес файла для записи данных.</param>
        /// <param name="leftBound">Индекс первого элемента данных для записи.</param>
        /// <param name="rightBound">Индекс последнего элемента данных для записи.</param>
        public void LoadToFile(string address, int leftBound, int rightBound)
        {
            string[] lines = new string[rightBound - leftBound + 1];
            lines[0] = "time,signal";
            // Если есть значения открытых каналов, они тоже должны быть записаны.
            if (OpenChannels != null)
                lines[0] += ",open_channels";
            for (int i = leftBound; i < rightBound; i++)
            {
                lines[i - leftBound + 1] = $"{Time[i].ToString("f4")},{Signal[i].ToString("f4")}";
                if (OpenChannels != null)
                    lines[i - leftBound + 1] += $",{OpenChannels[i]}";
            }
            File.WriteAllLines(address, lines);
        }
    }
}
