using System;
using System.IO;

namespace IonDataTreatment
{
    class DataTreater
    {
        /// <summary>
        /// Количество подряд идущих элементов в одном наборе (соответствует 50 секундам).
        /// </summary>
        const int BATCH = 500000;

        /// <summary>
        /// Основной метод программы, организующей предобработку данных.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            // Создаем объект с данными из train.csv.
            IonData data = new IonData();
            try
            {
                data.ReadFromFile(@"../../../../Data/RawData/train.csv");
            }
            catch (IOException)
            {
                Console.WriteLine($"Ошибка чтения файла. Проверьте наличие и настройки доступа файла train.csv.");
                Console.WriteLine("Нажмите любую клавишу...");
                Console.ReadKey(true);
                return;
            }
            catch (Exception)
            {
                Console.WriteLine("Ошибка обработки данных из файла train.csv. Скорее всего файл содержит данные неверного формата.");
                Console.WriteLine("Нажмите любую клавишу...");
                Console.ReadKey(true);
                return;
            }
            try
            {
                // Устраняем линейные и параболические сдвиги.
                data.RemoveLinearDrift(BATCH, BATCH + 100000, 3);
                data.RemoveParabolicDrift(6 * BATCH, 7 * BATCH, 5);
                data.RemoveParabolicDrift(7 * BATCH, 8 * BATCH, 5);
                data.RemoveParabolicDrift(8 * BATCH, 9 * BATCH, 5);
                data.RemoveParabolicDrift(9 * BATCH, 10 * BATCH, 5);
            }
            catch (IndexOutOfRangeException)
            {
                Console.WriteLine("При устранении сдвигов индекс вышел за пределы массива. Скорее всего файл train.csv содержит не верные данные.");
                Console.WriteLine("Нажмите любую клавишу...");
                Console.ReadKey(true);
                return;
            }
            catch (Exception ex)
            {
                Console.WriteLine("При устранении сдвигов произошла ошибка. Скорее всего файл train.csv содержит не верные данные.");
                Console.WriteLine("Текст ошибки:");
                Console.WriteLine(ex.Message);
                Console.WriteLine("Нажмите любую клавишу...");
                Console.ReadKey(true);
                return;
            }
            // Делим данные на 10 наборов и загружаем в соответствующие файлы.
            try
            {
                for (int i = 0; i < 10; i++)
                    data.LoadToFile(@$"../../../../Data/PreparedData/train{i + 1}.csv", BATCH * i, BATCH * (i + 1));
            }
            catch (Exception)
            {
                Console.WriteLine("Ошибка записи в файл. Проверьте настройки доступа файлов для результата.");
                Console.WriteLine("Нажмите любую клавишу...");
                Console.ReadKey(true);
                return;
            }

            // Проделываем тот же процесс для данных в test.csv, но не разбиваем результат на наборы.
            IonData testData = new IonData();
            try
            {
                testData.ReadFromFile(@"../../../../Data/RawData/test.csv");
            }
            catch (IOException)
            {
                Console.WriteLine($"Ошибка чтения файла. Проверьте наличие и настройки доступа файла test.csv.");
                Console.WriteLine("Нажмите любую клавишу...");
                Console.ReadKey(true);
                return;
            }
            catch (Exception)
            {
                Console.WriteLine("Ошибка обработки данных из файла test.csv. Скорее всего файл содержит данные неверного формата.");
                Console.WriteLine("Нажмите любую клавишу...");
                Console.ReadKey(true);
                return;
            }
            try 
            { 
                testData.RemoveLinearDrift(0, 100000, 3);
                testData.RemoveLinearDrift(100000, 200000, 3);
                testData.RemoveLinearDrift(400000, BATCH, 3);
                testData.RemoveLinearDrift(BATCH + 100000, BATCH + 200000, 3);
                testData.RemoveLinearDrift(BATCH + 200000, BATCH + 300000, 3);
                testData.RemoveLinearDrift(BATCH + 300000, BATCH + 400000, 3);
                testData.RemoveParabolicDrift(2 * BATCH, 3 * BATCH, 5);
            }
            catch (IndexOutOfRangeException)
            {
                Console.WriteLine("При устранении сдвигов индекс вышел за пределы массива. Скорее всего файл test.csv содержит не верные данные.");
                Console.WriteLine("Нажмите любую клавишу...");
                Console.ReadKey(true);
                return;
            }
            catch (Exception ex)
            {
                Console.WriteLine("При устранении сдвигов произошла ошибка. Скорее всего файл test.csv содержит не верные данные.");
                Console.WriteLine("Текст ошибки:");
                Console.WriteLine(ex.Message);
                Console.WriteLine("Нажмите любую клавишу...");
                Console.ReadKey(true);
                return;
            }
            try
            {
                testData.LoadToFile(@"../../../../Data/PreparedData/test.csv");
                Console.WriteLine("Предобработка данных прошла успешно. Для начала прогноза запустите приложение IonPrediction.");
                Console.WriteLine("Нажмите любую клавишу...");
                Console.ReadKey(true);
            }
            catch (Exception)
            {
                Console.WriteLine("Ошибка записи в файл. Проверьте настройки доступа файла test.csv.");
                Console.WriteLine("Нажмите любую клавишу...");
                Console.ReadKey(true);
            }
        }
    }
}
